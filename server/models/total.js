const TotalModel = function() {
	
	function saveTotal(total, mongoose, cb) {
		//create model from mongoose object
		let Total = mongoose.model('Total', { 'total': Number });

		//create new instance from model
		totalValue = new Total({ total: total });

		//call save on model instance and call the callack supplied from utils/processor.js to
		// return either total back to processor.js or the error
		// goto processor file functions callback
		totalValue.save(function(err) {
			if(err)	cb(null, err);

			else {
				cb(total, null);
			}
		});
	}

	return {
		saveTotal: saveTotal
	};
};

module.exports = TotalModel;