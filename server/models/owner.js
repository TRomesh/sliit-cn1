const OwnerModel = (function() {
	
	function foodS(firstname, lastname, username, password, sliitreg, mongoose, cb) {
		const food = mongoose.model('food', { 'id': Number, 'name': String, 'price': Number, 'qnty': Number });

		food.find(function (err, docs) {
      if (err) console.log(err);

      cb(null, docs);
    });
	}

	return {
		foodS: foodS
	};
})();

module.exports = OwnerModel;