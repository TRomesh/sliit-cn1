const express = require('express');
const app = express();
const mongoose = require('mongoose');
const PORT = process.env.PORT || 3000;
const router = express.Router();
const mongooseCreds = require('./config/mongoose');
const bodyParser = require('body-parser');
const path = require('path');

//routes
const indexRoutes = require('./routes/index');
const authRoutes = require('./routes/auth');

mongoose.connect(mongooseCreds);

app.use(router);

app.use('/', express.static('src'));

app.get('*', function(req, res) {
	res.sendFile(path.resolve('src', 'index.html'));
});

indexRoutes(router, bodyParser, mongoose);
authRoutes(router, bodyParser, mongoose);

app.listen(PORT, function() {
	console.log('server running on port', PORT);
});

module.exports.getApp = app;
