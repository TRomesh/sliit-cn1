const express = require('express');
const jwt = require('jsonwebtoken');
const auth = require('../utils/auth');
const ownerProc = require('../utils/owner');

const ownerRoutes = function(router, bodyParser, mongoose) {
	router.get('/api/v1/owner', bodyParser.json(), function (req, res) {
		ownerProc.foodS()
			.then(function(result) {
				res.json(result);
			})
			.catch(function(error) {
				res.json(error);
			});
	});
}

module.exports = ownerRoutes;