const express = require('express');
const jwt = require('jsonwebtoken');
const auth = require('../utils/auth');
const authModel = require('../models/auth');

const authRoutes = function(router, bodyParser, mongoose) {
	router.post('/api/v1/login',bodyParser.json(), function (req, res) {
		const username = req.body.username;
		const password = req.body.password;

		const token = jwt.sign({ foo: 'bar' }, 'sa-hack-sliit');
		
		auth.login(username, password)
			.then(function(result) {
				res.json(result);
			})
			.catch(function(error) {
				res.json(error);
			});
	})

	.post('/api/v1/register', bodyParser.json(), function(req, res) {
		const firstname = req.body.firstname;
		const lastname = req.body.firstname;
		const username = req.body.firstname;
		const password = req.body.firstname;
		const sliitreg = req.body.firstname;

		auth.register(firstname, lastname, username, password, sliitreg)
			.then(function(result) {

				authModel.register(firstname, lastname, username, password, sliitreg, mongoose, function(err, response) {
					console.log('error', err);
					if(err) {
						res.json({
							done: false
						});
					}
					else {
						console.log('db', res);
						res.json({
							done: response
						});
					}
				});
			})
			.catch(function(error) {
				res.json(error);
			});

	});
}

module.exports = authRoutes;