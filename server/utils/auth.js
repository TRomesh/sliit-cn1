const jwt = require('jsonwebtoken');
const loginHelper = require('../helpers/login');

const loginFacade = (function() {
	const verify = function() {
		return "done";
	}

	const login = function(username, password) {
		return new Promise(function(resolve, reject) {
			loginHelper.validateLogin(username, password, function(error, result) {
				try {
					const token = jwt.sign({ foo: 'bar' }, 'sa-hack-sliit');
					const resObj = {
						done: true,
						token: token
					};
					
					if(token !== undefined) {
						resolve(resObj);
					}
					else {
						reject('try again !');
					}
				}
				catch(err) {
					reject(err);
				}
			});
		});
	}

	const register = function(firstname, lastname, username, password, sliitreg) {
		return new Promise(function(resolve, reject) {
			loginHelper.validateRegister(firstname, lastname, username, password, sliitreg, function(error, result) {
				console.log('here');
				try {
					//if(error) reject(error);

					const resObj = {
						done: true
					};
					
					console.log('done');
					resolve(resObj);
				}
				catch(err) {
					reject(err);
				}
			});
		});
	}

	return {
		login: login,
		register: register,
		verify: verify
	};
})();

module.exports = loginFacade;
