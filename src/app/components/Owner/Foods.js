import React from 'react';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
var foods = [{
    id: 1,
    food: "Buns",
    price: 60,
    quantity: 100,
},{
    id: 2,
    food: "Pizza",
    price: 90,
    quantity: 10,
},{
    id: 3,
    food: "Burgers",
    price: 60,
    quantity: 20,
},{
    id: 4,
    food: "Yoghurt",
    price: 40,
    quantity: 100,
}];

export default class Foods extends React.Component {

    constructor(props) {
        super(props);

        fetch('/api/v1/owner/', {
            method: 'POST',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json'
            },
            body: JSON.stringify({
              username: 'Hubot',
              password: 'hubot',
            })
          }).then(function(response) {
            return response.json();
          })
          .then(function(res) {
            if(res.done) {
              localStorage.setItem('token', res.token);
              window.location = '/';
            }
          });
    }

    render() {
        return (
            <div>
                <BootstrapTable ref="table" className="col-lg-10" data={foods}   striped={true} hover={true} pagination={true} search={true}
                                exportCSV={true} height="360" csvFileName="Foods.csv" options={{paginationShowsTotal:true, sizePerPageList:[10] , sortName:"id", sortOrder:"desc"}}>
                    <TableHeaderColumn className="col-lg-2" dataField="id" isKey={true} dataAlign="center" dataSort={true}>ID</TableHeaderColumn>
                    <TableHeaderColumn className="col-lg-2" dataField="food" dataSort={true}>Food</TableHeaderColumn>
                    <TableHeaderColumn className="col-lg-2" dataField="price" dataSort={true}>Price(Rs.)</TableHeaderColumn>
                    <TableHeaderColumn className="col-lg-2" dataField="quantity" dataSort={true}>Quantity(Available)</TableHeaderColumn>
                </BootstrapTable>

            </div>
        );
    }
}