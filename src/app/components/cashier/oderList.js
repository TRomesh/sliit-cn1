import React from 'react';
import Avatar from 'material-ui/Avatar';
import {List, ListItem} from 'material-ui/List';
import Subheader from 'material-ui/Subheader';
import Divider from 'material-ui/Divider';
import baseTheme from 'material-ui/styles/baseThemes/lightBaseTheme';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import CommunicationChatBubble from 'material-ui/svg-icons/communication/chat-bubble';
import Paper from 'material-ui/Paper';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import RaisedButton from 'material-ui/RaisedButton';


const style = {
  height: 550,
  width: 400,
  margin: 20,
  textAlign: 'center',
  display: 'inline-block',
};




const orderData = [
 {
   img: 'images/grid-list/00-52-29-429_640.jpg',
   name: 'Tharaka',
   total:2323,
 },
 {
   img: 'images/grid-list/burger-827309_640.jpg',
   name: 'Madushika',
    total:423,
 },
 {
   img: 'images/grid-list/camera-813814_640.jpg',
   name: 'Rajika',
    total:273,
 },
 {
   img: 'images/grid-list/morning-819362_640.jpg',
   name: 'Isuru',
    total:283,
 },
 {
   img: 'images/grid-list/hats-829509_640.jpg',
   name:'Asber',
    total:2533,
 },
 {
   img: 'images/grid-list/honey-823614_640.jpg',
   name: 'Kasun',
    total:2565
 },
 {
   img: 'images/grid-list/vegetables-790022_640.jpg',
   name: 'Chathra',
    total:63,
 },
 {
   img: 'images/grid-list/water-plant-821293_640.jpg',
   name: 'Sahan',
   total:655
 },
];

const pendingOrderData = [
 {
   orid: 1,
   name: 'Tharaka',
   total:2323,
 },
 {
   orid:2,
   name: 'Madushika',
    total:423,
 },
 {
   orid:2,
   name: 'Rajika',
    total:273,
 },
 {
   orid:3,
   name: 'Isuru',
    total:283,
 },
 {
   orid:4,
   name:'Asber',
    total:2533,
 },
 {
   orid:5,
   name: 'Kasun',
    total:2565
 },
 {
   orid:6,
   name: 'Chathra',
    total:63,
 },
 {
   orid:7,
   name: 'Sahan',
   total:655
 },
];

export class oderList extends React.Component {

  constructor(props,context){
    super(props,context);

    this.state={
      Orders:[{Name:'Tharaka',Total:12},{Name:'Madushika',Total:23}]
    };

      fetch('/api/v1/cashier/pending', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          username: 'Hubot',
          password: 'hubot',
        })
      }).then(function(response) {
        return response.json();
      })
      .then(function(res) {
        if(res.done) {
          localStorage.setItem('token', res.token);
          window.location = '/';
        }
      });

 }

  getChildContext() {
      return { muiTheme: getMuiTheme(baseTheme) };
  }

   rejectOrder(event){
     event.preventDefault();

   }

   approveOrder(event){
     event.preventDefault();
   }

   caltot(){

   }

   handleToggle = (event, toggled) => {
    this.setState({
     [event.target.name]: toggled,
    });
   };

   handleChange = (event) => {
     this.setState({height: event.target.value});
   };

  render() {
    return (
      <div>
      <div className="col-md-7">
       <Paper zDepth={2} >
       <List>
      <Subheader>Recent Order</Subheader>
      {orderData.map((order,i)=>(
        <ListItem
          key={i}
          primaryText={order.name}
          leftAvatar={<Avatar src={order.img} />}
          rightIcon={<div>{order.total}</div>}
        />
      ))}
   </List>
    <div>
      <div className="col-md-4"><h3>Total</h3></div>
      <div className="col-md-4"></div>
      <div className="col-md-4"><h3>34343</h3></div>
    </div>
    </Paper>
   </div>
   <div className="col-md-5">
   <Paper  zDepth={2} >
   <Table>
    <TableHeader>
     <TableRow><p>Pending Orders</p></TableRow>
     <TableRow>
       <TableHeaderColumn>Order ID</TableHeaderColumn>
       <TableHeaderColumn>Customer</TableHeaderColumn>
       <TableHeaderColumn>Total</TableHeaderColumn>
     </TableRow>
     </TableHeader>
     <TableBody>
     {pendingOrderData.map((order,i)=>(
       <TableRow key={i}>
       <TableRowColumn>{order.id}</TableRowColumn>
       <TableRowColumn>{order.name}</TableRowColumn>
       <TableRowColumn>{order.total}</TableRowColumn>
       </TableRow>
     ))}

     </TableBody>
     </Table>
       <div className="row-md-4">
         <RaisedButton label="Save" primary={true} onTouchTap={this.approveOrder} className="row-md-4" />
         <RaisedButton label="Cancle" secondary={true} onTouchTap={this.rejectOrder} className="row-md-4"/>
       </div>
   </Paper>
   </div>
   </div>
    );
  }
}

oderList.childContextTypes = {
          muiTheme: React.PropTypes.object.isRequired
      };
