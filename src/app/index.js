import React from "react";
import { render } from "react-dom";
import { Router, Route, Link, browserHistory } from 'react-router';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import injectTapEventPlugin from 'react-tap-event-plugin';
import AppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import Drawer from './components/Sidebar/sidebar';
import Login from './components/Login/login';
import Foods from './components/Owner/Foods'

import 'whatwg-fetch';

injectTapEventPlugin();

import {Home} from "./components/Home";
import {oderList} from "./components/cashier/oderList";
import {Dashboard} from  "./components/Owner/Dashboard"
import {ItemList} from "./components/customer/ItemList";



const App =  React.createClass({
    getInitialState() {
      return {
        open: false
      }
    },

    componentDidMount: function() {
      this.setState({
        open: false
      });
    },


    _changeSideBarVisibility() {
      alert('sda');
    },

    _changeSideBarVisibility: function() {
      this.setState({
        open: !this.state.open
      });
    },

    render(){
      return (
        <div>
            <Drawer visibility={this.state.open} close={this._changeSideBarVisibility}/>
            <AppBar
                title="SLIIT CANTEEN"
                onLeftIconButtonTouchTap={this._changeSideBarVisibility}
                iconElementRight={
                    <IconMenu
                        iconButtonElement={
                            <IconButton>
                                <MoreVertIcon />
                            </IconButton>
                        }
                        targetOrigin={{horizontal: 'right', vertical: 'top'}}
                        anchorOrigin={{horizontal: 'right', vertical: 'top'}}>
                        <MenuItem primaryText="Settings" />
                        <MenuItem primaryText="Help" />
                        <MenuItem primaryText="Sign out"  />

                    </IconMenu>
                }
            />

            {this.props.children}
            </div>
     );
   }
});

render((
    <MuiThemeProvider>
      <Router history={browserHistory}>
        <Route path="/login" component={Login} />
        <Route path="/" component={App} >
          <Route path="/owner" component={Dashboard}/>
          <Route path="/oderList" component={oderList} />
          <Route path="/dashboard" component={Dashboard}/>
          <Route path="/foods" component={Foods}/>
          <Route path="/itemList" component={ItemList}/>

        </Route>
          
      </Router>
    </MuiThemeProvider>
), document.getElementById('app'));
